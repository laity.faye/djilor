<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('membres', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('num_membre')->unique();
            $table->string('prenom');
            $table->string('nom');
            $table->string('num_ci')->nullable()->unique();
            $table->string('telephone')->nullable()->unique();
            $table->text('adresse')->nullable();
            $table->date('date')->default('2024-01-01');
            $table->integer('remise')->default(0);
            $table->string('image')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('membres');
    }
};
