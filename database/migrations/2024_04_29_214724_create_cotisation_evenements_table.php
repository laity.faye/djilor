<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('cotisation_evenements', function (Blueprint $table) {
            $table->id();
            $table->double('montant');
            $table->date('date');
            $table->foreignId('membre_id')->constrained()->onDelete('cascade')->onUpdate('cascade');
            $table->foreignId('evenement_id')->constrained()->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('cotisation_evenements');
    }
};
