@extends('pdfs.app')
@section('content')
   
    <table id="synthese">
        <thead>
            <th style="color:black; text-transform: uppercase; font-weight: bold;background-color: #ffffff" 
            colspan="4">LISTE DES COTISATIONS MENSUELLES DU {{Carbon\Carbon::now()->format('m/Y')}}</th>
            <tr>
                <th>Membre</th>
                <th>Montant Cotiser</th>
                <th>Téléphone</th>
                <th>Date Cotisation</th>
            </tr>
        </thead>

        <tbody>

            @foreach ($cotisationM as $cotisation)
                <tr>
                    <td>{{ $cotisation->membre->full_name }}</td>
                    <td>{{ $cotisation->montant }}</td>
                    <td>{{ $cotisation->membre->telephone }}</td>
                    <td>{{Carbon\Carbon::createFromDate($cotisation->date)->format('d/m/Y')  }}</td>
                </tr>
            @endforeach

        </tbody>
        <tfoot>
            <tr>
                <td colspan="3"><strong>Total cotisationM</strong></td>
                <td>{{ $cotisationM->count() }}</td>
            </tr>
        </tfoot>
    </table>
    <br><br>
    <table id="synthese">
        <thead>
            <th style="color:black; text-transform: uppercase; font-weight: bold;background-color: #ffffff" 
            colspan="2">LISTE DES NON COTISANTS POUR LE MOIS DU {{Carbon\Carbon::now()->format('m/Y')}}</th>
            <tr>
                <th>Membre</th>
                <th>Téléphone</th>
            </tr>
        </thead>

        <tbody>

            @foreach ($membresNonCotises as $cotisation)
                <tr>
                    <td>{{ $cotisation->full_name }}</td>
                    <td>{{ $cotisation->telephone }}</td>
                </tr>
            @endforeach

        </tbody>
        <tfoot>
            <tr>
                <td><strong>Total des non cotisants</strong></td>
                <td>{{ $membresNonCotises->count() }}</td>
            </tr>
        </tfoot>
    </table>
@endsection