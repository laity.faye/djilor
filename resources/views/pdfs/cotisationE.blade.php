@extends('pdfs.app')
@section('content')
   
    <table id="synthese">
        <thead>
            <th style="color:black; text-transform: uppercase; font-weight: bold;background-color: #ffffff" 
            colspan="3">Liste des cotisations pour l'événement: <br> {{ $event->nom }} de {{$event->membre->full_name}} du {{ Carbon\Carbon::createFromDate($event->date)->format('d/m/Y') }}</th>
            <tr>
                <th>Nom du Membre</th>
                <th>Montant</th>
                <th>Date de Cotisation</th>
            </tr>
        </thead>

        <tbody>
            @php 
                $montant = 0;
            @endphp

            @foreach($cotisations as $cotisation)
                <tr>
                    <td>{{ $cotisation->membre->full_name }}</td>
                    <td>{{ $cotisation->montant }}</td>
                    <td>{{ Carbon\Carbon::createFromDate($cotisation->date)->format('d/m/Y') }}</td>
                </tr>
                @php 
                    $montant += $cotisation->montant;
                @endphp
                
            @endforeach

        </tbody>
        <tfoot>
            <tr>
                <td colspan="2"><strong>Total cotisation</strong></td>
                <td>{{$montant}}</td>
            </tr>
        </tfoot>
    </table>

    <br><br>
    <table id="synthese">
        <thead>
            <th style="color:black; text-transform: uppercase; font-weight: bold;background-color: #ffffff" 
            colspan="2">LISTE DES NON COTISANTS pour l'événement: <br> {{ $event->nom }} de {{$event->membre->full_name}} du {{ Carbon\Carbon::createFromDate($event->date)->format('d/m/Y') }}</th>
            <tr>
                <th>Membre</th>
                <th>Téléphone</th>
            </tr>
        </thead>

        <tbody>

            @foreach ($membresNonCotises as $cotisation)
                <tr>
                    <td>{{ $cotisation->full_name }}</td>
                    <td>{{ $cotisation->telephone }}</td>
                </tr>
            @endforeach

        </tbody>
        <tfoot>
            <tr>
                <td><strong>Total des non cotisants</strong></td>
                <td>{{ $membresNonCotises->count() }}</td>
            </tr>
        </tfoot>
    </table>
@endsection