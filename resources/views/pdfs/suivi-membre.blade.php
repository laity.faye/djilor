@extends('pdfs.app')
@section('content')
   
    <br>
    <table id="synthese">
        <thead>
            <th style="color:black; text-transform: uppercase; font-weight: bold;background-color: #ffffff" 
            colspan="5">Suivi de Membre</th>
            <tr>
                <th>N° Membre</th>
                <th>Nom</th>
                <th>Téléphone</th>
                <th>Adresse</th>
                <th>Date Adésion</th>
            </tr>
        </thead>

        <tbody>
            <tr>
                <td>{{ $membre->num_membre }}</td>
                <td>{{ $membre->full_name }}</td>
                <td>{{ $membre->telephone }}</td>
                <td>{!! $membre->adresse !!}</td>
                <td>{{Carbon\Carbon::createFromDate($membre->date)->format('d/m/Y')  }}</td>
            </tr>

        </tbody>
    </table>
   
    <table id="synthese">
        <thead>
            <th style="color:black; text-transform: uppercase; font-weight: bold;background-color: #ffffff" 
            colspan="2">Cotisations Mensuelles</th>
            <tr>
                <th>Montant Cotiser</th>
                <th>Date Cotisation</th>
            </tr>
        </thead>

        <tbody>

            @foreach ($cotisationsMensuelles as $cotisation)
                <tr>
                    <td>{{ $cotisation->montant }}</td>
                    <td>{{ \Carbon\Carbon::parse($cotisation->date)->format('d/m/Y') }}</td>
                </tr>
            @endforeach

        </tbody>
        <tfoot>
            <tr>
                <td><strong>Total cotisation Mensuelles</strong></td>
                <td>{{ $cotisationsMensuelles->count() }}</td>
                
            </tr>
        </tfoot>
    </table>
   
    <table id="synthese">
        <thead>
            <th style="color:black; text-transform: uppercase; font-weight: bold;background-color: #ffffff" 
            colspan="3">Cotisations aux Événements</th>
            <tr>
                <th>Événement</th>
                <th>Date</th>
                <th>Montant</th>
            </tr>
        </thead>

        <tbody>

            @foreach($cotisationsEvenements as $evenement)
            @foreach($evenement->cotisationEvenements as $cotisation)
            <tr>
                <td>{{ $evenement->nom }}</td>
                <td>{{ \Carbon\Carbon::parse($cotisation->date)->format('d/m/Y') }}</td>
                <td>{{ $cotisation->montant }}</td>
            </tr>
            @endforeach
            @endforeach
        </tbody>
        <tfoot>
            <tr>
                <td colspan="2"><strong>Total cotisation Evenements</strong></td>
                <td>{{ $cotisationsEvenements->count() }}</td>
                
            </tr>
        </tfoot>
    </table>
    
    <table id="synthese">
        <thead>
            <th style="color:black; text-transform: uppercase; font-weight: bold;background-color: #ffffff" 
            colspan="2">Les Non Cotisés</th>
        </thead>
        <tbody>
            <tr>
                <td>
                <table id="synthese">
                    <thead>
                        <tr>
                            <th>Mois Non Cotisés</th>
                        </tr>
                    </thead>
                    @foreach($moisNonCotises as $mois)
                    <tr>
                        <td>{{ \Carbon\Carbon::parse($mois . '-01')->format('m/Y') }}</td>
                    </tr>
                    @endforeach

                    <tr>
                        <td><strong>Total: {{ $moisNonCotises->count() }} </strong></td>  
                    </tr>
                    
                </table>
            </td>
            <td>
                <table id="synthese">
                    <thead>
                        <th style="color:black; text-transform: uppercase; font-weight: bold;background-color: #ffffff" 
                        colspan="2">Événements Non Cotisés</th>
                        <tr>
                            <th>Événement</th>
                            <th>Date</th>
                        </tr>
                    </thead>
                    @foreach($evenementsNonCotises as $evenement)
                    <tr>
                        <td>{{ $evenement->nom }}</td>
                        <td>{{ \Carbon\Carbon::parse($evenement->date)->format('d/m/Y') }}</td>
                    </tr>
                    @endforeach
                    <tr>
                        <td><strong>Total</strong></td>
                        <td>{{ $evenementsNonCotises->count() }}</td>
                    </tr>
                </table>
            </td>
            </tr>
            
        </tbody>
    </table>
  
@endsection