@extends('pdfs.app')
@section('content')
   
    <table id="synthese">
        <thead>
            <th style="color:black; text-transform: uppercase; font-weight: bold;background-color: #ffffff" 
            colspan="3">LISTE DES EVENEMENTS DU {{Carbon\Carbon::now()->format('m/Y')}}</th>
            <tr>
                <th>Evénement</th>
                <th>Membre</th>
                <th>Date Evénement</th>
            </tr>
        </thead>

        <tbody>

            @foreach ($evenements as $evenement)
                <tr>
                    <td>{{ $evenement->nom }}</td>
                    <td>{{ $evenement->membre->full_name }}</td>
                    <td>{{Carbon\Carbon::createFromDate($evenement->date)->format('d/m/Y')  }}</td>
                </tr>
            @endforeach

        </tbody>
        <tfoot>
            <tr>
                <td colspan="2"><strong>Total Evénement</strong></td>
                <td>{{ $evenement->count() }}</td>
            </tr>
        </tfoot>
    </table>
@endsection