<html lang=""{{ str_replace('_', '-', app()->getLocale()) }}"">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="css/app.css">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('title')</title>
</head>
<body>
    <section>
        <div class="info">
            <div class="info_a">
                <div class="logo">
                    <img src="img/inno.png">
                </div>
                <div class="uj">
                    <img src="img/u.png">
                </div>
                <div class="sigle">
                    <p>Union Des Jeunes de Ngaraf (Djilor)</p>
                </div>
                <div class="contact">
                    <div class="label">Contacts</div>
                    <div class="addr">Adresse: Djilor</div>
                    <div class="addr"><span>Trésorier: </span><code>Amadou Doudou Mbodji</code></div>
                    <div class="addr"><span>Num OrangeMoney: </span> <code>78 018 62 29</code></div>
                    <div class="addr"><span>Num Wave: </span> <code>78 018 62 29</code></div>
                </div>
            </div>
        </div>
    </section>
    <br><br>
    <section>
        <div class="container">
            @yield('content')
        </div>
    </section>
</body>
</html>
