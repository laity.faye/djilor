@extends('pdfs.app')
@section('content')
   
    <table id="synthese">
        <thead>
            <th style="color:black; text-transform: uppercase; font-weight: bold;background-color: #ffffff" 
            colspan="5">LISTE DES MEMBRES</th>
            <tr>
                <th>N° Membre</th>
                <th>Nom</th>
                <th>Téléphone</th>
                <th>Adresse</th>
                <th>Date Adésion</th>
            </tr>
        </thead>

        <tbody>

            @foreach ($membres as $membre)
                <tr>
                    <td>{{ $membre->num_membre }}</td>
                    <td>{{ $membre->full_name }}</td>
                    <td>{{ $membre->telephone }}</td>
                    <td>{!! $membre->adresse !!}</td>
                    <td>{{Carbon\Carbon::createFromDate($membre->date)->format('d/m/Y')  }}</td>
                </tr>
            @endforeach

        </tbody>
        <tfoot>
            <tr>
                <td colspan="4"><strong>Total membres</strong></td>
                <td>{{ $membres->count() }}</td>
            </tr>
        </tfoot>
    </table>
@endsection