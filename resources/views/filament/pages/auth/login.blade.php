<div>
        <x-filament-panels::page.simple>
            @if (filament()->hasRegistration())
                <x-slot name="subheading">
                    {{ __('filament-panels::pages/auth/login.actions.register.before') }}
        
                    {{ $this->registerAction }}
                </x-slot>
            @endif
        
            {{ \Filament\Support\Facades\FilamentView::renderHook(\Filament\View\PanelsRenderHook::AUTH_LOGIN_FORM_BEFORE, scopes: $this->getRenderHookScopes()) }}
        
            <x-filament-panels::form wire:submit="authenticate">
                {{ $this->form }}
        
                <x-filament-panels::form.actions
                    :actions="$this->getCachedFormActions()"
                    :full-width="$this->hasFullWidthFormActions()"
                />
            </x-filament-panels::form>
        
            {{ \Filament\Support\Facades\FilamentView::renderHook(\Filament\View\PanelsRenderHook::AUTH_LOGIN_FORM_AFTER, scopes: $this->getRenderHookScopes()) }}
        </x-filament-panels::page.simple>
        
        <div style="text-align: center; font-size: 13px; margin-top: 30px">
            © Copyright InnoSoft Creations,tous droits révervés<br>Thiés None Derrière le CEM <br>Coordonnées: <span style="color: #0066ffff"><a href="linkedin.com/in/laity-faye-588b15272" target="_blank">Laity FAYE</a></span><br>Email: <span style="color: #0066ffff">laity.faye@univ-thies.sn</span><br>Tel: <span style="color: #0066ffff">+221 78 018 62 29</span>
        </div>
    </div>