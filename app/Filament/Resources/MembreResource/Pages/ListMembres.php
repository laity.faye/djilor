<?php

namespace App\Filament\Resources\MembreResource\Pages;

use App\Filament\Resources\MembreResource;
use Filament\Actions;
use Filament\Actions\Action;
use Filament\Resources\Pages\ListRecords;

class ListMembres extends ListRecords
{
    protected static string $resource = MembreResource::class;
    protected static ?string $breadcrumb = "Liste des Membres";

    protected function getHeaderActions(): array
    {
        return [
            Action::make('imprimer un pdf')
                ->label('Imprimer tous les membres')
                ->button()
                ->url(fn () => route('membre-pdf'), shouldOpenInNewTab: true),
            Actions\CreateAction::make()->label('Nouveau Membre'),
        ];
    }
}
