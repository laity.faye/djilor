<?php

namespace App\Filament\Resources\MembreResource\Pages;

use App\Filament\Resources\MembreResource;
use Filament\Actions;
use Filament\Resources\Pages\CreateRecord;
use Filament\Actions\Action;
use Filament\Notifications\Notification;

use Illuminate\Contracts\Support\Htmlable;

class CreateMembre extends CreateRecord
{
    protected static string $resource = MembreResource::class;
    protected static ?string $breadcrumb = "Création de Membre";
    protected function getRedirectUrl(): string
    {
        return static::getResource()::getUrl('index');
    }

    protected function getCreatedNotificationTitle(): ?string
    {
        return "Membre créé";
    }
    protected function getCreatedNotification(): Notification
    {
        $title = $this->getCreatedNotificationTitle();

        if (blank($title)) {
            return null;
        }
        return Notification::make()
            ->success()
            ->title($this->getCreatedNotificationTitle())
            ->body('Création réussi.');
    }

    public function getTitle(): string | Htmlable
    {
        if (filled(static::$title)) {
            return static::$title;
        }

        return "Création de Membre";
    }

    protected function getCreateFormAction(): Action
    {
        return Action::make('create')
            ->label("Créer")
            ->submit('create')
            ->keyBindings(['mod+s']);
    }

    protected function getCancelFormAction(): Action
    {
        return Action::make('cancel')
            ->label("Annuler")
            ->url($this->previousUrl ?? static::getResource()::getUrl())
            ->color('gray');
    }

    /**
     * @return array<Action | ActionGroup>
     */
    protected function getFormActions(): array
    {
        return [
            $this->getCreateFormAction(),
            $this->getCancelFormAction(),
        ];
    }
    public function mutateFormDataBeforeCreate(array $data): array{
        $data['telephone'] = $data['telephone']!=""? str_replace(' ', '', $data['telephone']):null;
        return $data;
    }
}
