<?php

namespace App\Filament\Resources;

use App\Filament\Resources\EvenementResource\Pages;
use App\Filament\Resources\EvenementResource\RelationManagers;
use App\Models\Evenement;
use App\Models\Membre;
use Filament\Forms;
use Filament\Forms\Components\DatePicker;
use Filament\Forms\Components\Group;
use Filament\Forms\Components\Section;
use Filament\Forms\Components\Select;
use Filament\Forms\Components\TextInput;
use Filament\Forms\Form;
use Filament\Resources\Resource;
use Filament\Tables;
use Filament\Tables\Table;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletingScope;

class EvenementResource extends Resource
{
    protected static ?string $model = Evenement::class;

    protected static ?string $navigationIcon = 'heroicon-o-rectangle-stack';

    public static function form(Form $form): Form
    {
        return $form
        ->schema([
            Group::make([
                Section::make([
                    Select::make('membre_id')
                        ->options(Membre::all()->pluck('full_info', 'id'))
                        ->label('Membre')
                        ->searchable()
                        ->required(),
                    TextInput::make('nom')
                        ->label('Evénement')
                        ->required(),
                    DatePicker::make('date')
                        ->label('Date de l\'événement'),

                ])->columns(['sm' => 2])
            ])
        ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                Tables\Columns\TextColumn::make("#")
                    ->state(
                        static function (Tables\Contracts\HasTable $livewire, \stdClass $rowLoop): string {
                            return (string) (
                                $rowLoop->iteration + (intval($livewire->getTableRecordsPerPage()) * ($livewire->getTablePage() - 1))
                            );
                        }
                    ),
                Tables\Columns\TextColumn::make('nom')->searchable()->searchable()->label('Evénement'),
                Tables\Columns\TextColumn::make('membre.full_name')->alignCenter()->label('Membre'),
                Tables\Columns\TextColumn::make('date')->searchable()->alignCenter()->date(format: 'l j M Y')->label('Date de l\'événement'),
            ])
            ->filters([
                //
            ])
            ->actions([
                Tables\Actions\Action::make('Imprimer')
                ->icon('heroicon-o-document-arrow-down')
                ->button()
                ->outlined()
                ->url(fn (Evenement $record) => route('cotisationE-pdf', [$record]), shouldOpenInNewTab: true),
                Tables\Actions\EditAction::make(),
                Tables\Actions\DeleteAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\BulkActionGroup::make([
                    Tables\Actions\DeleteBulkAction::make(),
                ]),
            ]);
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListEvenements::route('/'),
            'create' => Pages\CreateEvenement::route('/create'),
            'edit' => Pages\EditEvenement::route('/{record}/edit'),
        ];
    }
}
