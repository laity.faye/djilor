<?php

namespace App\Filament\Resources\CotisationEvenementResource\Pages;

use App\Filament\Resources\CotisationEvenementResource;
use Filament\Actions;
use Filament\Actions\Action;
use Filament\Resources\Pages\ListRecords;

class ListCotisationEvenements extends ListRecords
{
    protected static string $resource = CotisationEvenementResource::class;

    protected static ?string $breadcrumb = "Liste des Cotisations";
    protected static ?string $title = "Cotisation Evénement";

    protected function getHeaderActions(): array
    {
        return [
            Actions\CreateAction::make()->label("Nouvelle Cotisation"),
        ];
    }
}
