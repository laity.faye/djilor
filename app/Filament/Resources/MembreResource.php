<?php

namespace App\Filament\Resources;

use App\Filament\Resources\MembreResource\Pages;
use App\Filament\Resources\MembreResource\RelationManagers;
use App\Models\Membre;
use Filament\Forms;
use Filament\Forms\Components\DatePicker;
use Filament\Forms\Components\FileUpload;
use Filament\Forms\Components\Textarea;
use Filament\Forms\Components\TextInput;
use Filament\Forms\Form;
use Filament\Resources\Resource;
use Filament\Tables;
use Filament\Tables\Table;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletingScope;


class MembreResource extends Resource
{
    protected static ?string $model = Membre::class;

    protected static ?string $navigationIcon = 'heroicon-o-user-group';

    public static function form(Form $form): Form
    {
        return $form
        ->schema([
            Forms\Components\Section::make()
                ->schema([
                    TextInput::make('nom')->label('Nom')
                        ->required()
                        ->maxLength(255),
                    TextInput::make('prenom')->label('Prénom')
                        ->required()
                        ->maxLength(255),
                    TextInput::make('telephone')->label('Numéro Téléphone')
                        ->required()
                        ->regex('/^(?:\s*)(77|70|76|78|75)(?:\s*)([0-9]){3}(?:\s*)([0-9]){2}(?:\s*)([0-9]){2}(?:\s*)$/i'),
                    TextInput::make('num_ci')->label('Numéro CI')
                        ->regex('/^[0-9]{13}$/i')
                        ->nullable()
                        ->numeric(),
                    TextInput::make('num_membre')->label('Numéro Membre')
                        ->numeric()
                        ->default(fn () => Membre::max('id') + 1)
                        ->readOnly(),
                    DatePicker::make('date')
                        ->required()
                        ->native(false)
                        ->label('Date Adésion'),
                    Textarea::make('adresse')->label('Adresse')
                        ->nullable(),
                    FileUpload::make('image')
                    ->image()
                    ->imageEditor()
                ])->columns(['sm' => 4]),

        ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                Tables\Columns\ImageColumn::make('image')->alignCenter()->label('Image'),
                Tables\Columns\TextColumn::make('num_membre')->searchable()->label('Num Membre'),
                Tables\Columns\TextColumn::make('prenom')->searchable()->label('Prénom'),
                Tables\Columns\TextColumn::make('nom')->searchable()->label('Nom'),
                Tables\Columns\TextColumn::make('telephone')->label('Téléphone')->formatStateUsing(fn (string $state) => substr($state, 0, 2) . " " . substr($state, 2, 3) . " " . substr($state, 5, 2) . " " . substr($state, 7, 2)),
                Tables\Columns\TextColumn::make('adresse')->label('Adresse'),
                Tables\Columns\TextColumn::make('date')->label('Date Adésion'),
            ])
            
            ->actions([
                Tables\Actions\Action::make('suivi-membre')
                ->icon('heroicon-o-document-arrow-down')
                ->button()
                ->outlined()
                ->url(fn (Membre $record) => route('suivi-membre-pdf', [$record]), shouldOpenInNewTab: true),
                Tables\Actions\EditAction::make(),
                Tables\Actions\DeleteAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\BulkActionGroup::make([
                    Tables\Actions\DeleteBulkAction::make(),
                ]),
            ]);
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListMembres::route('/'),
            'create' => Pages\CreateMembre::route('/create'),
            'view' => Pages\ViewMembre::route('/{record}'),
            'edit' => Pages\EditMembre::route('/{record}/edit'),
        ];
    }
}
