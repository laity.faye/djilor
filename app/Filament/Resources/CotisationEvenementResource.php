<?php

namespace App\Filament\Resources;

use App\Filament\Resources\CotisationEvenementResource\Pages;
use App\Models\CotisationEvenement;
use App\Models\Evenement;
use App\Models\Membre;
use Closure;
use Filament\Forms\Components\DatePicker;
use Filament\Forms\Components\Group;
use Filament\Forms\Components\Section;
use Filament\Forms\Components\Select;
use Filament\Forms\Components\TextInput;
use Filament\Forms\Form;
use Filament\Forms\Get;
use Filament\Resources\Resource;
use Filament\Tables;
use Filament\Tables\Table;

class CotisationEvenementResource extends Resource
{
    protected static ?string $model = CotisationEvenement::class;

    protected static ?string $navigationIcon = 'heroicon-o-banknotes';
    protected static ?string $navigationLabel = "Cotisation Evenement";


    public static function form(Form $form): Form
    {
        return $form
        ->schema([
            Group::make([
                Section::make([
                    Select::make('membre_id')
                        ->options(Membre::all()->pluck('full_info', 'id'))
                        ->label('Membre')
                        ->searchable()
                        ->required(),
                    Select::make('evenement_id')
                        ->options(Evenement::all()->pluck('full_info', 'id'))
                        ->label('Evenement')
                        ->searchable()
                        ->required(),
                    TextInput::make('montant')
                        ->label('Montant a Cotiser')
                        ->required(),
                    DatePicker::make('date')
                        ->label('Date du Cotisation')
                        ->required()
                        ->rule(function (Get $get) {
                            return function (string $attribute, $value, Closure $fail) use ($get) {
                                if (strpos($_SERVER['HTTP_REFERER'], '/create') || strpos(request()->url(), '/create')){
                                    $membreId = $get('membre_id');
                                    $evenementId = $get('evenement_id');
                                    $date = $value;
                                    $existingCotisations = \App\Models\CotisationEvenement::where('membre_id', $membreId)
                                        ->where('evenement_id', $evenementId)
                                        ->whereYear('date', date('Y', strtotime($date)))
                                        ->whereMonth('date', date('m', strtotime($date)))
                                        ->exists();
                        
                                    if ($existingCotisations) {
                                        $fail('Ce membre a déjà cotisé pour cet Evénement.');
                                    }
                                }
                            };
                        })

                ])->columns(['sm' => 2])
            ])
        ])->columns(['sm' => 1]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                Tables\Columns\TextColumn::make("#")
                ->state(
                    static function (Tables\Contracts\HasTable $livewire, \stdClass $rowLoop): string {
                        return (string) (
                            $rowLoop->iteration + (intval($livewire->getTableRecordsPerPage()) * ($livewire->getTablePage() - 1))
                        );
                    }
                ),
            Tables\Columns\TextColumn::make('evenement.nom')->searchable()->label('Evénement'),
            Tables\Columns\TextColumn::make('evenement.date')->searchable()->date(format: 'l j M Y')->label('Date Evénement'),
            Tables\Columns\TextColumn::make('evenement.membre.full_name')->label('Bénéficiaire'),
            Tables\Columns\TextColumn::make('membre.full_name')->alignCenter()->label('Contributeur'),
            Tables\Columns\TextColumn::make('montant')->searchable()->label('Montant Cotisé'),
            Tables\Columns\TextColumn::make('date')->searchable()->alignCenter()->date(format: 'l j M Y')->label('Date Cotisation'),
            ])
            ->filters([
                //
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
                Tables\Actions\DeleteAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\BulkActionGroup::make([
                    Tables\Actions\DeleteBulkAction::make(),
                ]),
            ]);
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListCotisationEvenements::route('/'),
            'create' => Pages\CreateCotisationEvenement::route('/create'),
            'edit' => Pages\EditCotisationEvenement::route('/{record}/edit'),
        ];
    }
}
