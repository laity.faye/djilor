<?php

namespace App\Filament\Resources;

use App\Filament\Resources\CotisationmensuelleResource\Pages;
use App\Models\Cotisationmensuelle;
use App\Models\Membre;
use Closure;
use Filament\Forms\Components\DatePicker;
use Filament\Forms\Components\Group;
use Filament\Forms\Components\Section;
use Filament\Forms\Components\Select;
use Filament\Forms\Components\TextInput;
use Filament\Forms\Form;
use Filament\Forms\Get;
use Filament\Resources\Resource;
use Filament\Tables;
use Filament\Tables\Table;


class CotisationmensuelleResource extends Resource
{
    protected static ?string $model = Cotisationmensuelle::class;

    protected static ?string $navigationIcon = 'heroicon-o-banknotes';
    protected static ?string $navigationLabel = "Cotisation Mensuelle";


    public static function form(Form $form): Form
    {
        return $form
        ->schema([
            Group::make([
                Section::make([
                    Select::make('membre_id')
                        ->options(Membre::all()->pluck('full_info', 'id'))
                        ->label('Membre')
                        ->searchable()
                        ->required(),
                    TextInput::make('montant')
                        ->label('Montant a Cotiser')
                        ->required(),
                    DatePicker::make('date')
                        ->label('Date du Cotisation')
                        ->required()
                        ->rule(function (Get $get) {
                            return function (string $attribute, $value, Closure $fail) use ($get) {
                                if (strpos($_SERVER['HTTP_REFERER'], '/create') || strpos(request()->url(), '/create')){
                                    $membreId = $get('membre_id');
                                    $date = $value;
                                    $existingCotisations = \App\Models\Cotisationmensuelle::where('membre_id', $membreId)
                                        ->whereYear('date', date('Y', strtotime($date)))
                                        ->whereMonth('date', date('m', strtotime($date)))
                                        ->exists();
                        
                                    if ($existingCotisations) {
                                        $fail('Ce membre a déjà cotisé pour ce mois.');
                                    }
                                }
                            };
                        })

                ])->columns(['sm' => 2])
            ])
        ])->columns(['sm' => 1]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                Tables\Columns\TextColumn::make("#")
                ->state(
                    static function (Tables\Contracts\HasTable $livewire, \stdClass $rowLoop): string {
                        return (string) (
                            $rowLoop->iteration + (intval($livewire->getTableRecordsPerPage()) * ($livewire->getTablePage() - 1))
                        );
                    }
                ),
            Tables\Columns\TextColumn::make('membre.full_name')->alignCenter()->label('Membre'),
            Tables\Columns\TextColumn::make('montant')->searchable()->label('Montant Cotiser'),
            Tables\Columns\TextColumn::make('date')->searchable()->alignCenter()->date(format: 'l j M Y')->label('Date Cotisation'),
            ])
            
            ->actions([
                Tables\Actions\EditAction::make(),
                Tables\Actions\DeleteAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\BulkActionGroup::make([
                    Tables\Actions\DeleteBulkAction::make(),
                ]),
            ]);
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListCotisationmensuelles::route('/'),
            'create' => Pages\CreateCotisationmensuelle::route('/create'),
            'edit' => Pages\EditCotisationmensuelle::route('/{record}/edit'),
        ];
    }
}
