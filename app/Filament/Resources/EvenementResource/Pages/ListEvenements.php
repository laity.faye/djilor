<?php

namespace App\Filament\Resources\EvenementResource\Pages;

use App\Filament\Resources\EvenementResource;
use Filament\Actions;
use Filament\Actions\Action;
use Filament\Resources\Pages\ListRecords;

class ListEvenements extends ListRecords
{
    protected static string $resource = EvenementResource::class;
    protected static ?string $breadcrumb = "Liste des Evénement";

    protected function getHeaderActions(): array
    {
        return [
            Action::make('imprimer un pdf')
            ->label('Imprimer Tous Les Evénement')
            ->button()
            ->url(fn () => route('evenement-pdf'), shouldOpenInNewTab: true),
            Actions\CreateAction::make()->label("Nouvel événement"),
        ];
    }
}
