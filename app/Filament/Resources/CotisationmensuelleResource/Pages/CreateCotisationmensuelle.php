<?php

namespace App\Filament\Resources\CotisationmensuelleResource\Pages;

use App\Filament\Resources\CotisationmensuelleResource;
use Filament\Actions;
use Filament\Actions\Action;
use Filament\Notifications\Notification;
use Filament\Resources\Pages\CreateRecord;
use Illuminate\Contracts\Support\Htmlable;

class CreateCotisationmensuelle extends CreateRecord
{
    protected static string $resource = CotisationmensuelleResource::class;
    protected static ?string $breadcrumb = "Création d'une Cotisation Mensuelle";
    protected function getRedirectUrl(): string
    {
        return static::getResource()::getUrl('index');
    }

    protected function getCreatedNotificationTitle(): ?string
    {
        return "Cotisation créée";
    }
    protected function getCreatedNotification(): Notification
    {
        $title = $this->getCreatedNotificationTitle();

        if (blank($title)) {
            return null;
        }
        return Notification::make()
            ->success()
            ->title($this->getCreatedNotificationTitle())
            ->body('Création réussie.');
    }

    public function getTitle(): string | Htmlable
    {
        if (filled(static::$title)) {
            return static::$title;
        }

        return "Création d'une Cotisation";
    }

    protected function getCreateFormAction(): Action
    {
        return Action::make('create')
            ->label("Créer")
            ->submit('create')
            ->keyBindings(['mod+s']);
    }

    protected function getCancelFormAction(): Action
    {
        return Action::make('cancel')
            ->label("Annuler")
            ->url($this->previousUrl ?? static::getResource()::getUrl())
            ->color('gray');
    }

    /**
     * @return array<Action | ActionGroup>
     */
    protected function getFormActions(): array
    {
        return [
            $this->getCreateFormAction(),
            $this->getCancelFormAction(),
        ];
    }
}
