<?php

namespace App\Filament\Resources\CotisationmensuelleResource\Pages;

use App\Filament\Resources\CotisationmensuelleResource;
use Filament\Actions;
use Filament\Actions\Action;
use Filament\Resources\Pages\ListRecords;

class ListCotisationmensuelles extends ListRecords
{
    protected static string $resource = CotisationmensuelleResource::class;
    protected static ?string $breadcrumb = "Liste des Cotisations";
    protected static ?string $title = "Cotisation Mensuelle";
   
    protected function getHeaderActions(): array
    {
        return [
            Action::make('imprimer un pdf')
            ->label('Imprimer tous les cotisations')
            ->button()
            ->url(fn () => route('cotisationM-pdf'), shouldOpenInNewTab: true),
            Actions\CreateAction::make()->label("Nouvelle Cotisation Mensuelle"),
        ];
    }
}
