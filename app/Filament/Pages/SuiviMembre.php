<?php

namespace App\Filament\Pages;

use Filament\Forms\Components\Actions;
use Filament\Forms\Components\Actions\Action;
use Filament\Forms\Components\DatePicker;
use Filament\Forms\Components\Group;
use Filament\Forms\Components\Section;
use Filament\Forms\Form;
use Filament\Forms\Get;
use Filament\Support\Enums\ActionSize;
use Filament\Pages\Page;

class SuiviMembre extends Page
{
    protected static ?string $navigationIcon = 'heroicon-o-user';

    protected static string $view = 'filament.pages.suivi-membre';
    public $startDate = null;
    public ?array $data = [];
    protected static ?string $title = "Suivi Cotisation Mensuelle";

    public function mount(): void
    {
        $this->form->fill();
    }
    public function form(Form $form): Form
    {
        return $form
            ->schema([
                Group::make([
                    Section::make([
                        Group::make([
                            DatePicker::make('startDate')
                                ->required()
                                ->columnSpan(1)
                                ->label('Sélectionner le Mois')
                                ->afterStateUpdated(fn (Get $get) => $this->startDate = $get('startDate') != null && $get('startDate') != "" ? $get('startDate') : null)
                                ->live(),
    
                        ]),
                        Actions::make([
                            Action::make('save')
                                ->size(ActionSize::ExtraLarge)
                                ->disabled(fn()=>!$this->startDate)
                                ->label('Lancer La Synthèse')
                                ->url(fn () => $this->sendToPdf(), shouldOpenInNewTab: true)
                        ])

                    ]),
                ]),
            ])->statePath('data')->columns(2);
    }

    public function sendToPdf()
    {
        if ($this->startDate) {

            return route('suivi-pdf', parameters:json_encode($this->startDate));
        }
        return null;
    }
}
