<?php
namespace App\Filament\Widgets;

use Filament\Widgets\ChartWidget;
use App\Models\Cotisationmensuelle;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class CotisationChart extends ChartWidget
{
    protected static ?string $heading = 'Variation des Cotisations Mensuelles';
    protected static ?int $sort = 1;
    protected static string $color = 'info';

    protected function getData(): array
    {
        $currentYear = Carbon::now()->year;
        $cotisations = Cotisationmensuelle::select(
            DB::raw('SUM(montant) as total'),
            DB::raw('MONTH(date) as month'),
            DB::raw('YEAR(date) as year')
        )
        ->whereYear('date', $currentYear)
        ->groupBy('year', 'month')
        ->orderBy('month', 'asc')
        ->get();

        $data = [];
        $labels = [];

        $months = [];
        for ($i = 1; $i <= 12; $i++) {
            $months[$i] = [
                'month' => $i,
                'year' => $currentYear,
                'total' => 0
            ];
        }

        foreach ($cotisations as $cotisation) {
            if (isset($months[$cotisation->month])) {
                $months[$cotisation->month]['total'] = $cotisation->total;
            }
        }

        foreach ($months as $key => $value) {
            $data[] = $value['total'];
            $labels[] = Carbon::create($value['year'], $value['month'], 1)->translatedFormat('F');
        }

        return [
            'datasets' => [
                [
                    'label' => 'Total des Cotisations',
                    'data' => $data,
                    'backgroundColor' => 'rgba(75, 192, 192, 0.2)',
                    'borderColor' => 'rgba(75, 192, 192, 1)',
                    'borderWidth' => 1,
                ],
            ],
            'labels' => $labels,
        ];
    }

    protected function getType(): string
    {
        return 'line';
    }
}
