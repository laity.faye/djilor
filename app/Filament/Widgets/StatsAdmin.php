<?php

namespace App\Filament\Widgets;

use App\Models\Cotisationmensuelle;
use App\Models\Evenement;
use App\Models\Membre;
use Carbon\Carbon;
use Filament\Widgets\StatsOverviewWidget as BaseWidget;
use Filament\Widgets\StatsOverviewWidget\Stat;

class StatsAdmin extends BaseWidget
{
    protected function getStats(): array
    {
        $currentMonthEventCount = Evenement::whereBetween('date', [Carbon::now()->startOfMonth(), Carbon::now()->endOfMonth()])->count();
        $cotisation = Cotisationmensuelle::whereBetween('date', [Carbon::now()->startOfMonth(), Carbon::now()->endOfMonth()])->count();
        $CotisationSum = Cotisationmensuelle::whereBetween('date', [Carbon::now()->startOfMonth(), Carbon::now()->endOfMonth()])->sum('montant');
       
        return [
            Stat::make('Membres', (Membre::count()))
                ->description('Membres Enregistrées')
                ->descriptionIcon('heroicon-m-arrow-trending-up')
                ->chart([7, 2, 10, 3, 15, 4, 17])
                ->color('primary')
                ->url(route('filament.admin.resources.membres.index')),
            Stat::make('Nombre d\'Evénements Du Mois', $currentMonthEventCount)
                ->description('Evénements Enregistrées')
                ->descriptionIcon('heroicon-m-arrow-trending-up')
                ->chart([7, 2, 10, 3, 15, 4, 17])
                ->color('primary')
                ->url(route('filament.admin.resources.evenements.index')),
            Stat::make('Nombre De Cotisant Du Mois',  $cotisation)
                ->description('Cotisation Enregistrées')
                ->descriptionIcon('heroicon-m-arrow-trending-up')
                ->chart([7, 2, 10, 3, 15, 4, 17])
                ->color('primary')
                ->url(route('filament.admin.resources.cotisationmensuelles.index')),
            Stat::make('Montant des Cotisations Du Mois',  $CotisationSum.'F')
                ->description('Enregistrées')
                ->descriptionIcon('heroicon-m-arrow-trending-up')
                ->chart([7, 2, 10, 3, 15, 4, 17])
                ->color('primary')
                ->url(route('filament.admin.resources.cotisationmensuelles.index')),

        ];
    }
}
