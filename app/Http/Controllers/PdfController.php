<?php

namespace App\Http\Controllers;
use App\Models\Cotisationmensuelle;
use App\Models\Evenement;
use App\Models\Membre;
use Barryvdh\DomPDF\Facade\Pdf;
use Carbon\Carbon;

class PdfController extends Controller
{
    public function generateSuivi($data)
    {
        $date = json_decode(($data), true);
        $startDate = Carbon::parse($date);
        $cotisations = Cotisationmensuelle::whereYear('date', $startDate->year)->whereMonth('date', $startDate->month)->get();
        $membres = Membre::all();
        $membresCotises = $cotisations->pluck('membre_id');
        $membresNonCotises = $membres->whereNotIn('id', $membresCotises);
        $pdf = Pdf::loadView('pdfs.cotisation_mois', ['data' => $cotisations, 'dates' => $startDate, 'membresNonCotises' => $membresNonCotises])->setPaper('a4', 'portrait');
        $pdf->download('cotisation-mensuelle.pdf');
        return $pdf->stream('syntheseMensuelle.pdf');
    }

    public function generateMembres()
    {
        $pdf = Pdf::loadView('pdfs.membres', ['membres' => Membre::all()])->setPaper('a4', 'portrait');
        $pdf->download('liste_Membres.pdf');
        return  $pdf->stream('liste_membres.pdf');
    }

    public function generateCotisationM()
    {
        $moisActuel = date('m');
        $anneeActuelle = date('Y');
    
        $cotisationsM = Cotisationmensuelle::whereYear('date', $anneeActuelle)
            ->whereMonth('date', $moisActuel)
            ->get();
        $membres = Membre::all();
        $membresCotises = $cotisationsM->pluck('membre_id');
        $membresNonCotises = $membres->whereNotIn('id', $membresCotises);

        $pdf = Pdf::loadView('pdfs.cotisationM', [
            'cotisationM' => $cotisationsM,
            'membresNonCotises' => $membresNonCotises
        ])->setPaper('a4', 'portrait');
    
        $pdf->download('liste_cotisation_mensuelle.pdf');
        return  $pdf->stream('liste_cotisation_mensuelle.pdf');
    }
    

    public function generateCotisationE(Evenement $record)
    {
        
        $cotisations = $record->cotisationEvenements()->with('membre')->get();
        $membres = Membre::all();
        $membresCotises = $cotisations->pluck('membre_id');
        $membresNonCotises = $membres->whereNotIn('id', $membresCotises);
        
        $pdf = Pdf::loadView('pdfs.cotisationE', [
            'event' => $record,
            'cotisations' => $cotisations,
            'membresNonCotises' => $membresNonCotises
        ])->setPaper('a4', 'portrait');

        return $pdf->stream('liste_cotisation_evenement.pdf');
    }

    public function generateEvenement()
    {
        $pdf = Pdf::loadView('pdfs.evenement', ['evenements' => Evenement::all()])->setPaper('a4', 'portrait');
        $pdf->download('liste_Evenements.pdf');
        return  $pdf->stream('liste_Evenements.pdf');
    }

    public function generateSuiviMembre(Membre $record)
    {
        $cotisationsMensuelles = $record->cotisationm()->get();
        $cotisationsEvenements = $record->evenements()->with('cotisationEvenements')->get();
        $moisCotises = $cotisationsMensuelles->pluck('date')->map(function ($date) {
            return Carbon::parse($date)->format('Y-m');
        })->unique();
        $start = $moisCotises->first() ? Carbon::parse($moisCotises->first()) : Carbon::now();
        $end = Carbon::now();
        $allMonths = collect();
        while ($start->lessThanOrEqualTo($end)) {
            $allMonths->push($start->format('Y-m'));
            $start->addMonth();
        }
        $moisNonCotises = $allMonths->diff($moisCotises);
        $tousLesEvenements = Evenement::all();
        $evenementsNonCotises = $tousLesEvenements->diff($cotisationsEvenements);

        $pdf = Pdf::loadView('pdfs.suivi-membre', [
            'membre' => $record,
            'cotisationsMensuelles' => $cotisationsMensuelles,
            'cotisationsEvenements' => $cotisationsEvenements,
            'moisNonCotises' => $moisNonCotises,
            'evenementsNonCotises' => $evenementsNonCotises,
        ])->setPaper('a4', 'portrait');

        return $pdf->stream('suivi_membre.pdf');
    }
    

}
