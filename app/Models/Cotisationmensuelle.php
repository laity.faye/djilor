<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cotisationmensuelle extends Model
{
    use HasFactory;
    protected $table = 'cotisationmensuelles';
    public $timestamps = true;

    protected $fillable = [
        'montant',
        'date',
        'membre_id',
    ];

    public function membre()
    {
        return $this->belongsTo(Membre::class);
    }
}
