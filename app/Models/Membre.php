<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Membre extends Model
{
    use HasFactory;
    protected $table = 'membres';
    public $timestamps = true;
    public $append = ['full_name', 'full_info'];

    protected $fillable = [
        'num_membre',
        'prenom',
        'nom',
        'num_ci',
        'telephone',
        'adresse',
        'date',
        'remise',
        'image',
    ];

    public function getFullInfoAttribute()
    {
        return ucfirst($this->num_membre) . ', ' . ucfirst($this->prenom) . ' ' . strval($this->nom);
    }

    public function getFullNameAttribute()
    {
        return ucfirst($this->prenom) . ' ' . ucfirst($this->nom);
    }

    public function evenements()
    {
        return $this->hasMany(Evenement::class);
    }

    public function cotisationm()
    {
        return $this->hasMany(Cotisationmensuelle::class);
    }

}
