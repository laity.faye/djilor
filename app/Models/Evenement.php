<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Evenement extends Model
{
    use HasFactory;
    protected $table = 'evenements';
    public $timestamps = true;
    public $append = 'full_info';

    protected $fillable = [
        'nom',
        'date',
        'membre_id',
    ];

    public function membre()
    {
        return $this->belongsTo(Membre::class);
    }

    public function cotisationEvenements()
    {
        return $this->hasMany(CotisationEvenement::class);
    }

    public function getFullInfoAttribute()
    {
        $membre = Membre::find($this->membre_id);

        if ($membre) {
            return ucfirst($this->date) . ', ' . ucfirst($this->nom) . ' ' . $membre->full_name;
        }
    }

}
