<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CotisationEvenement extends Model
{
    use HasFactory;
    protected $table = 'cotisation_evenements';
    public $timestamps = true;

    protected $fillable = [
        'montant',
        'date',
        'membre_id',
        'evenement_id',
    ];

    public function membre()
    {
        return $this->belongsTo(Membre::class);
    }

    public function evenement()
    {
        return $this->belongsTo(Evenement::class);
    }
}
