<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PdfController;

Route::get('/', function () {
    return redirect('/admin');
});

Route::controller(PdfController::class)->group(
    function () {
        Route::get('/cotisation_mois/{data}', 'generateSuivi')->name('suivi-pdf');
        Route::get('/membres', 'generateMembres')->name('membre-pdf');
        Route::get('/cotisation-mensuelle', 'generateCotisationM')->name('cotisationM-pdf');
        Route::get('/{record}/cotisation-evenement', 'generateCotisationE')->name('cotisationE-pdf');
        Route::get('/evenement-pdf', 'generateEvenement')->name('evenement-pdf');
        Route::get('/{record}/suivi-membre', 'generateSuiviMembre')->name('suivi-membre-pdf');
        
    }
);