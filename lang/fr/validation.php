<?php
return [

/*
|--------------------------------------------------------------------------
| Validation Language Lines
|--------------------------------------------------------------------------
|
| The following language lines contain the default error messages used by
| the validator class. Some of these rules have multiple versions such
| as the size rules. Feel free to tweak each of these messages here.
|
*/

'accepted' => 'Le champ :attribute doit être accepté.',
'accepted_if' => 'Le champ :attribute doit être accepté quand :other est :value.',
'active_url' => 'Le champ :attribute doit contenir une URL valide.',
'after' => 'Le champ :attribute doit contenir une date postérieure au :date.',
'after_or_equal' => 'Le champ :attribute doit contenir une date postérieure ou égale à :date.',
'alpha' => 'Le champ :attribute ne peut contenir que des lettres.',
'alpha_dash' => 'Le champ :attribute ne peut contenir que des lettres, des chiffres, des tirets et des underscores.',
'alpha_num' => 'Le champ :attribute ne peut contenir que des chiffres et des lettres.',
'array' => 'Le champ :attribute doit être un tableau.',
'ascii' => 'Le champ :attribute ne peut contenir que des caractères alphanumériques ASCII.',
'before' => 'Le champ :attribute doit contenir une date antérieure au :date.',
'before_or_equal' => 'Le champ :attribute doit contenir une date antérieure ou égale à :date.',
'between' => [
    'array' => 'Le tableau :attribute doit contenir entre :min et :max éléments.',
    'file' => 'Le fichier :attribute doit être compris entre :min et :max kilo-octets.',
    'numeric' => 'Le champ :attribute doit être compris entre :min et :max.',
    'string' => 'Le texte :attribute doit contenir entre :min et :max caractères.',
],
'boolean' => 'Le champ :attribute doit être vrai ou faux.',
'can' => 'Le champ :attribute renferme une valeur non autorisée.',
'confirmed' => 'Le champ de confirmation :attribute ne correspond pas.',
'current_password' => 'Le mot de passe est incorrect.',
'date' => 'Le champ :attribute n\'est pas une date valide.',
'date_equals' => 'Le champ :attribute doit contenir la date :date.',
'date_format' => 'Le champ :attribute ne correspond pas au format :format.',
'decimal' => 'Le champ :attribute doit avoir :decimal décimales.',
'declined' => 'Le champ :attribute doit être refusé.',
'declined_if' => 'Le champ :attribute doit être refusé quand :other est :value.',
'different' => 'Les champs :attribute et :other doivent être différents.',
'digits' => 'Le champ :attribute doit comporter :digits chiffres.',
'digits_between' => 'Le champ :attribute doit comporter entre :min et :max chiffres.',
'dimensions' => 'L\'image :attribute possède des dimensions non valides.',
'distinct' => 'Le champ :attribute comporte une valeur en double.',
'doesnt_end_with' => 'Le champ :attribute ne peut pas se terminer par l\'un des éléments suivants : :values.',
'doesnt_start_with' => 'Le champ :attribute ne peut pas commencer par l\'un des éléments suivants : :values.',
'email' => 'Le champ :attribute doit contenir une adresse email valide.', 
'ends_with' => 'Le champ :attribute doit se terminer par l\'un des éléments suivants : :values.',
'enum' => 'L\'élément :attribute sélectionné est invalide.',
'exists' => 'L\'élément :attribute sélectionné est invalide.',
'file' => 'Le champ :attribute doit être un fichier.',
'filled' => 'Le champ :attribute est obligatoire.',
'gt' => [
    'array' => 'Le tableau :attribute doit contenir plus de :value éléments.',
    'file' => 'Le fichier :attribute doit être supérieur à :value kilo-octets.',
    'numeric' => 'Le champ :attribute doit être supérieur à :value.',
    'string' => 'Le texte :attribute doit contenir plus de :value caractères.',
],
'gte' => [
    'array' => 'Le tableau :attribute doit contenir au moins :value éléments.',
    'file' => 'Le fichier :attribute doit être supérieur ou égal à :value kilo-octets.',
    'numeric' => 'Le champ :attribute doit être supérieur ou égal à :value.',
    'string' => 'Le texte :attribute doit contenir au moins :value caractères.',
],
'image' => 'Le champ :attribute doit être une image.',
'in' => 'L\'élément :attribute sélectionné est invalide.',
'in_array' => 'Le champ :attribute n\'existe pas dans :other.',
'integer' => 'Le champ :attribute doit être un entier.',
'ip' => 'Le champ :attribute doit contenir une adresse IP valide.',
'ipv4' => 'Le champ :attribute doit contenir une adresse IPv4 valide.',
'ipv6' => 'Le champ :attribute doit contenir une adresse IPv6 valide.',
'json' => 'Le champ :attribute doit être une chaîne JSON valide.',
'lowercase' => 'Le champ :attribute doit être en minuscules.',
'lt' => [
    'array' => 'Le tableau :attribute doit contenir moins de :value éléments.',
    'file' => 'Le fichier :attribute doit être inférieur à :value kilo-octets.',
    'numeric' => 'Le champ :attribute doit être inférieur à :value.',
    'string' => 'Le texte :attribute doit contenir moins de :value caractères.',
],
'lte' => [
    'array' => 'Le tableau :attribute ne doit pas contenir plus de :value éléments.',
    'file' => 'Le fichier :attribute doit être inférieur ou égal à :value kilo-octets.',
    'numeric' => 'Le champ :attribute doit être inférieur ou égal à :value.',
    'string' => 'Le texte :attribute doit contenir au plus :value caractères.',
],
'mac_address' => 'Le champ :attribute doit contenir une adresse MAC valide.',
'max' => [
    'array' => 'Le tableau :attribute ne peut pas contenir plus de :max éléments.',
    'file' => 'Le fichier :attribute ne peut pas dépasser :max kilo-octets.',
    'numeric' => 'Le champ :attribute ne peut pas être supérieur à :max.',
    'string' => 'Le texte :attribute ne peut pas contenir plus de :max caractères.',
],
'max_digits' => 'Le champ :attribute ne peut pas contenir plus de :max chiffres.',
'mimes' => 'Le champ :attribute doit être un fichier de type : :values.',
'mimetypes' => 'Le champ :attribute doit être un fichier de type : :values.',
'min' => [
    'array' => 'Le tableau :attribute doit contenir au moins :min éléments.',
    'file' => 'Le fichier :attribute doit faire au moins :min kilo-octets.', 
    'numeric' => 'Le champ :attribute doit être au moins :min.',
    'string' => 'Le texte :attribute doit contenir au moins :min caractères.',
],
'min_digits' => 'Le champ :attribute doit contenir au moins :min chiffres.',
'missing' => 'Le champ :attribute doit être manquant.',
'missing_if' => 'Le champ :attribute doit être manquant quand :other est :value.',
'missing_unless' => 'Le champ :attribute doit être manquant à moins que :other soit :value.',  
'missing_with' => 'Le champ :attribute doit être manquant quand :values est présent.',
'missing_with_all' => 'Le champ :attribute doit être manquant quand :values sont présents.',
'multiple_of' => 'Le champ :attribute doit être un multiple de :value',
'not_in' => 'L\'élément :attribute sélectionné n\'est pas valide.',
'not_regex' => 'Le format du champ :attribute n\'est pas valide.',
'numeric' => 'Le champ :attribute doit contenir un nombre.',
'password' => [
    'letters' => 'Le champ :attribute doit contenir au moins une lettre.',
    'mixed' => 'Le champ :attribute doit contenir au moins une majuscule et une minuscule.',
    'numbers' => 'Le champ :attribute doit contenir au moins un chiffre.',
    'symbols' => 'Le champ :attribute doit contenir au moins un symbole.',
    'uncompromised' => 'Le :attribute donné a déjà été divulgué lors d\'une fuite de données. Veuillez en choisir un autre.',
],
'present' => 'Le champ :attribute doit être présent.',
'present_if' => 'Le champ :attribute doit être présent quand :other est :value.', 
'present_unless' => 'Le champ :attribute doit être présent sauf si :other est :value.',
'present_with' => 'Le champ :attribute doit être présent quand :values est présent.',
'present_with_all' => 'Le champ :attribute doit être présent quand :values sont présents.',
'prohibited' => 'Le champ :attribute est interdit.',
'prohibited_if' => 'Le champ :attribute est interdit quand :other est :value.',
'prohibited_unless' => 'Le champ :attribute est interdit sauf si :other est dans :values.',
'prohibits' => 'Le champ :attribute interdit la présence de :other.',
'regex' => 'Le format du champ :attribute est invalide.',
'required' => 'Le champ :attribute est obligatoire.',
'required_array_keys' => 'Le tableau :attribute doit contenir des entrées pour :values.',
'required_if' => 'Le champ :attribute est obligatoire quand :other est :value.',
'required_if_accepted' => 'Le champ :attribute est obligatoire quand :other est accepté.',
'required_unless' => 'Le champ :attribute est obligatoire sauf si :other est dans :values.',
'required_with' => 'Le champ :attribute est obligatoire quand :values est présent.',
'required_with_all' => 'Le champ :attribute est obligatoire quand :values sont présents.',
'required_without' => 'Le champ :attribute est obligatoire quand :values n\'est pas présent.',
'required_without_all' => 'Le champ :attribute est requis quand aucun de :values n\'est présent.',
'same' => 'Les champs :attribute et :other doivent correspondre.',
'size' => [
    'array' => 'Le tableau :attribute doit contenir :size éléments.',
    'file' => 'Le fichier :attribute doit faire :size kilo-octets.',
    'numeric' => 'Le champ :attribute doit être égal à :size.',
    'string' => 'Le texte :attribute doit contenir :size caractères.',
],
'starts_with' => 'Le champ :attribute doit commencer par l\'un des éléments suivants: :values',
'string' => 'Le champ :attribute doit être une chaîne de caractères.',
'timezone' => 'Le champ :attribute doit être un fuseau horaire valide.',
'unique' => 'La valeur du champ :attribute est déjà utilisée.',
'uploaded' => 'Le fichier du champ :attribute n\'a pas pu être téléversé.',
'uppercase' => 'Le champ :attribute doit être en majuscules.',
'url' => 'Le format du champ :attribute est invalide.',
'ulid' => 'Le champ :attribute doit contenir un ULID valide.', 
'uuid' => 'Le champ :attribute doit contenir un UUID valide.',

/*
|--------------------------------------------------------------------------
| Custom Validation Language Lines
|--------------------------------------------------------------------------
|
| Here you may specify custom validation messages for attributes using the
| convention "attribute.rule" to name the lines. This makes it quick to
| specify a specific custom language line for a given attribute rule.
|
*/

'custom' => [
    'attribute-name' => [
        'rule-name' => 'custom-message',
    ],
],

/*
|--------------------------------------------------------------------------
| Custom Validation Attributes
|--------------------------------------------------------------------------
|
| The following language lines are used to swap our attribute placeholder
| with something more reader friendly such as "E-Mail Address" instead
| of "email". This simply helps us make our message more expressive.
|
*/

'attributes' => [],

];